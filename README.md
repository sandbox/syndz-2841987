<h2>Introduction</h2>
This sandbox module allows you to import your (custom) path aliases by extending the <a href="https://www.drupal.org/project/default_content" title="Default content">default content</a> module.

As of right now, path aliases are not exported automatically because they are not (part of) an entity.
The following issues are aiming to solve the problem:
<ul>
  <li><a href="https://www.drupal.org/node/2649646" title="[pp-1] Normalize path fields as part of the entity: allow REST clients to know the path alias">[pp-1] Normalize path fields as part of the entity: allow REST clients to know the path alias</a></li>
  <li><a href="https://www.drupal.org/node/2336597" title="Convert path aliases to full featured entities">Convert path aliases to full featured entities</a></li>
</ul>

This module does not aim to be a solution, merely a workaround until the problem has been fixed in Drupal core.

<h2>Requirements</h2>
<ul>
  <li><a href="https://www.drupal.org/project/default_content" title="Default content">Default content</a></li>
</ul>

<h2>Installation</h2>
See <a href="https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8#adv" title="Installing contributed modules: Find, import, enable, configure - Drupal 8">Installing contributed modules: Find, import, enable, configure - Drupal 8</a> for more information about installing sandbox modules.

<h2>Configuration</h2>
The module has no configuration. The path alias will be found in the exported JSON file in <code>_links.self.href</code>.