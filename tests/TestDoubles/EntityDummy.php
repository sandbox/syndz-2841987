<?php

namespace Drupal\default_path_aliases\Tests\TestDoubles;

use Drupal\Core\Entity\Entity;

class EntityDummy extends Entity {

  public function __construct() {
    // Stub.
  }
}