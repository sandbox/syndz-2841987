<?php

namespace Drupal\default_path_aliases\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\default_content\Event\DefaultContentEvents;
use Drupal\default_content\Event\ImportEvent;
use Drupal\default_path_aliases\Entity\ImportedEntity;
use Drupal\pathauto\PathautoState;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      DefaultContentEvents::IMPORT => ['onContentImport']
    ];
  }

  /**
   * @param \Drupal\default_content\Event\ImportEvent $importEvent
   */
  public static function onContentImport(ImportEvent $importEvent) {
    foreach ($importEvent->getImportedEntities() as $entity) {
      $importedEntity = new ImportedEntity($importEvent, $entity);
      if ($pathAlias = $importedEntity->getPathAlias()) {
        try {
          if (self::entityHasPathAlias($entity, $pathAlias)) {
            self::createPathAliasForEntity($entity, $pathAlias);
          }
        } catch (UndefinedLinkTemplateException $exception) {
          // Intentionally left empty in case an entity type does not support urls.
        }
      }
    }
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $pathAlias
   * @return bool
   */
  private static function entityHasPathAlias(EntityInterface $entity, $pathAlias) {
    return $pathAlias !== self::getInternalPathByEntity($entity);
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @return string
   */
  private static function getInternalPathByEntity(EntityInterface $entity) {
    return "/{$entity->toUrl()->getInternalPath()}";
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $pathAlias
   */
  private static function createPathAliasForEntity(EntityInterface $entity, $pathAlias) {
    $language = $entity->language()->getId();
    \Drupal::service('path.alias_storage')
      ->save(self::getInternalPathByEntity($entity), $pathAlias, $language);

    $customPathAlias = self::entityHasCustomPathAlias($entity, $pathAlias);
    $entity->path->pathauto = $customPathAlias ? PathautoState::SKIP : PathautoState::CREATE;
    $entity->save();
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $pathAlias
   * @return bool
   */
  private static function entityHasCustomPathAlias(EntityInterface $entity, $pathAlias) {
    $pathAutoAlias = \Drupal::service('pathauto.generator')
      ->createEntityAlias($entity, 'return');

    return $pathAlias !== $pathAutoAlias;
  }
}