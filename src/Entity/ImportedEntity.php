<?php

namespace Drupal\default_path_aliases\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\default_content\Event\ImportEvent;
use Drupal\default_path_aliases\Exception\InvalidPathAliasException;
use Drupal\default_path_aliases\Exception\UnknownEntityFilePathException;

class ImportedEntity {
  /** @var string */
  private $importedEntityContentBasePath;
  /** @var \Drupal\Core\Entity\EntityInterface */
  private $entity;

  /**
   * @param \Drupal\default_content\Event\ImportEvent $importEvent
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function __construct(ImportEvent $importEvent, EntityInterface $entity) {
    $modulePath = drupal_get_path('module', $importEvent->getModule());
    $this->importedEntityContentBasePath = DRUPAL_ROOT . "/{$modulePath}/content";
    $this->entity = $entity;
  }

  /**
   * @return string
   * @throws \Drupal\default_path_aliases\Exception\InvalidPathAliasException
   */
  public function getPathAlias() {
    if ($importedEntityPathAlias = $this->getImportedEntityPath()) {
      $pathAlias = str_replace($GLOBALS['base_url'], '', $importedEntityPathAlias);

      list($pathAlias) = explode('?', $pathAlias);
      if (strpos($pathAlias, '/') === 0) {
        return $pathAlias;
      }

      throw new InvalidPathAliasException("A path alias is required to start with a slash, \"{$pathAlias}\" did not match this criteria.");
    }

    return FALSE;
  }

  /**
   * @return bool
   */
  private function getImportedEntityPath() {
    $entityData = $this->getImportedEntityRestData();

    if (isset($entityData['_links']['self']['href'])) {
      return $entityData['_links']['self']['href'];
    }

    return FALSE;
  }

  /**
   * @return string|bool
   */
  private function getImportedEntityRestData() {
    return Json::decode(file_get_contents($this->getImportedEntityFilePath()));
  }

  /**
   * @return string
   * @throws \Drupal\default_path_aliases\Exception\UnknownEntityFilePathException
   */
  public function getImportedEntityFilePath() {
    $entityType = $this->entity->getEntityTypeId();
    $uuid = $this->entity->uuid();
    $importedEntityFilePath = "{$this->importedEntityContentBasePath}/{$entityType}/{$uuid}.json";

    if (file_exists($importedEntityFilePath)) {
      return $importedEntityFilePath;
    }

    throw new UnknownEntityFilePathException("Entity file of type \"{$entityType}\" with uuid \"{$uuid}\" does not exist.");
  }
}