<?php

namespace Drupal\default_path_aliases\Tests {

  use Drupal\default_content\Event\ImportEvent;
  use Drupal\default_path_aliases\Entity\ImportedEntity;
  use Drupal\default_path_aliases\Tests\TestDoubles\EntityDummy;
  use Drupal\default_path_aliases\Tests\TestDoubles\EntityMock;

  class ImportedEntityTest extends \PHPUnit_Framework_TestCase {

    /**
     * Set global variables.
     */
    public function setUp() {
      $GLOBALS['base_url'] = 'http://default';
    }

    /**
     * @test
     */
    public function canInstantiateAnImportedEntity() {
      new ImportedEntity(new ImportEvent([], 'module'), new EntityDummy());
    }

    /**
     * @test
     * @expectedException \Drupal\default_path_aliases\Exception\UnknownEntityFilePathException
     * @expectedExceptionMessage Entity file of type "node" with uuid "non-existing-uuid" does not exist.
     */
    public function nonExistingEntityFileThrowsException() {
      $entity = new ImportedEntity(new ImportEvent([], 'module'), new EntityMock('node', 'non-existing-uuid'));
      $entity->getImportedEntityFilePath();
    }

    /**
     * @test
     */
    public function canFindEntityFilePathByEntityObject() {
      $entityTypeId = 'node';
      $uuid = '0d3b42ec-e324-475c-9239-cd3ac853426d';
      $entity = new ImportedEntity(new ImportEvent([], 'module'), new EntityMock($entityTypeId, $uuid));

      $expected = __DIR__ . "/fixtures/content/{$entityTypeId}/{$uuid}.json";
      $this->assertEquals($expected, $entity->getImportedEntityFilePath());
    }

    /**
     * @test
     * @expectedException \Drupal\default_path_aliases\Exception\InvalidPathAliasException
     * @expectedExceptionMessage A path alias is required to start with a slash, "http://example.com/parent-path/custom-url-path" did not match this criteria.
     */
    public function givenAliasWithoutSlashAtBeginningThrowsException() {
      $entityTypeId = 'node';
      $uuid = 'entity-exported-from-another-domain';
      $entity = new ImportedEntity(new ImportEvent([], 'module'), new EntityMock($entityTypeId, $uuid));
      $entity->getPathAlias();
    }

    /**
     * @test
     */
    public function canGetPathAliasOfEntityFile() {
      $entityTypeId = 'node';
      $uuid = '0d3b42ec-e324-475c-9239-cd3ac853426d';
      $entity = new ImportedEntity(new ImportEvent([], 'module'), new EntityMock($entityTypeId, $uuid));

      $this->assertEquals('/parent-path/custom-url-path', $entity->getPathAlias());
    }
  }
}
namespace {
  const DRUPAL_ROOT = __DIR__;

  function drupal_get_path() {
    return 'fixtures';
  }
}