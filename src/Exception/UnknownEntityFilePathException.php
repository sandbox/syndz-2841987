<?php

namespace Drupal\default_path_aliases\Exception;

class UnknownEntityFilePathException extends \Exception {
}