<?php

namespace Drupal\default_path_aliases\Tests\TestDoubles;

class EntityMock extends EntityDummy {
  /** @var string */
  private $uuid;

  /**
   * EntityMock constructor.
   * @param $entityTypeId
   * @param $uuid
   */
  public function __construct($entityTypeId, $uuid) {
    $this->entityTypeId = $entityTypeId;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId() {
    return $this->entityTypeId;
  }

  /**
   * {@inheritdoc}
   */
  public function uuid() {
    return $this->uuid;
  }
}